import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CountriesComponent } from './components/country/countries/countries.component';
import { FindCountryComponent } from './components/country/find-country/find-country.component';
import { HeaderComponent } from './components/template/header/header.component';
import { FooterComponent } from './components/template/footer/footer.component';
import { BodyComponent } from './components/template/body/body.component';

import { HttpClientModule } from "@angular/common/http";

/* RUTAS **/
import { AppRoutingModule } from './app.routes';
import { InfoCountryComponent } from './components/country/info-country/info-country.component';
import { CardCountryComponent } from './components/country/card-country/card-country.component';
import { AboutComponent } from './components/country/about/about.component';


@NgModule({
  declarations: [
    AppComponent,
    CountriesComponent,
    FindCountryComponent,
    HeaderComponent,
    FooterComponent,
    BodyComponent,
    InfoCountryComponent,
    CardCountryComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable } from '@angular/core';

import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  constructor( private http: HttpClient ) { 
     console.log('Servicio CountriesService');
  }


  private getQueryCountries(query: string){
    console.log('Metodo getQueryCountries');
    const URL = "https://restcountries.eu/rest/v2"
    return this.http.get(`${ URL }/${query}`);
  }

  getAllCountries() {
    console.log('Metodo getAllCountries');
    return this.getQueryCountries('all');
  }

  getCountryByName(name: string){
    return this.getQueryCountries(`name/${name}`);
  }

  getCountryByCode(code: string){
    return this.getQueryCountries(`alpha/${code}`);
  }
}

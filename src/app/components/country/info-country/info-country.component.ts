import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CountriesService } from '../../services/countries.service';

@Component({
  selector: 'app-info-country',
  templateUrl: './info-country.component.html',
  styleUrls: ['./info-country.component.css']
})
export class InfoCountryComponent implements OnInit {

  country: any = {};
  query: string;
  currency: number = 3;

  constructor( private activatedRoute: ActivatedRoute, private countriesService: CountriesService) { }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe(param => {
      this.query = param['name'];
      // console.log('La rta es ' + JSON.stringify(param)+ JSON.stringify(this.country));
    }, error => {
      console.log(`Error ${error.message} `)
    });

     if( this.query != undefined ){
        this.countriesService.getCountryByCode(this.query).subscribe((data: any) => {
          this.country = data;
          console.log('DATA ' + JSON.stringify(this.country));
        }, error => {
          console.log(`Error ${error.message} `)
        });
      }
      console.log('COUNTRY ' + JSON.stringify(this.country));

  }

}

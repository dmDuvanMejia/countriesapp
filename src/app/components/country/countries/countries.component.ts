import { Component, OnInit } from '@angular/core';
import { CountriesService } from '../../services/countries.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent {

   countries: any[] = [];


  constructor( private countriesServices: CountriesService ) {
    console.log('Constructor');
    this.countriesServices.getAllCountries().subscribe( (data: any) => {
      this.countries = data;
    });
  }


}

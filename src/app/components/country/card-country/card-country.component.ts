import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-country',
  templateUrl: './card-country.component.html',
  styleUrls: ['./card-country.component.css']
})
export class CardCountryComponent implements OnInit {

  @Input() countries: any = {};

  constructor( private route: Router) { }

  ngOnInit(): void {
    // console.log('CARD COUNTRY NGONINIT ' + JSON.stringify(this.countries) );
  }

  viewInfoCountry(name: string){
    console.log('viewInfoCountry Name ' + name);
    this.route.navigate(['info', name]);
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CountriesService } from '../../services/countries.service';

@Component({
  selector: 'app-find-country',
  templateUrl: './find-country.component.html',
  styleUrls: ['./find-country.component.css']
})
export class FindCountryComponent implements OnInit {

  countries: any[] = [];
  query: string;

  constructor( private activatedRoute: ActivatedRoute, private countriesService: CountriesService) { 
  }


  ngOnInit(): void {
    //  console.log('FIND COUNTRY NGONINIT ' + JSON.stringify(this.countries) );
    this.activatedRoute.params.subscribe( params => {
      this.query = params['query']; 
      this.countriesService.getCountryByName(this.query).subscribe( (data: any) => {
        this.countries = data;
        console.log('La data consultada es ' + JSON.stringify(this.countries));
      }, error => {
        console.log(`Eror ${error.message} Completo => ${JSON.stringify(error)} `);
        this.countries = [];
      })
      console.log('El valor del parametro es ' + JSON.stringify(params));
    });
  }

  

}


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router
import { BodyComponent } from './components/template/body/body.component';
import { CountriesComponent } from './components/country/countries/countries.component';
import { FindCountryComponent } from './components/country/find-country/find-country.component';
import { InfoCountryComponent } from './components/country/info-country/info-country.component';
import { AboutComponent } from './components/country/about/about.component';

const routes: Routes = [
    { path: '', component: BodyComponent },
    { path: 'home',  component: BodyComponent },
    { path: 'countries', component: CountriesComponent },
    { path: 'findcountry/:query', component: FindCountryComponent },
    { path: 'info/:name', component: InfoCountryComponent }, 
    { path: 'about', component: AboutComponent }, 
    { path: '**', pathMatch: 'full', redirectTo: 'home' },

]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }